# CKI Yum Repo

RPM repositories for the CKI packages.

## How does it work?

This repository creates yum repositories using rpm packages generated at
others repositories, using the features [needs] and [trigger] from Gitlab CI.

That way, any time those packages get updated at their repos, their pipelines
will trigger this repo's pipeline, updating the yum repository.

The pipeline from this project will always use the rpm stored as artifacts at
the main branch (from the last job) from the upstream projects.

[needs]: https://docs.gitlab.com/ee/ci/yaml/README.html#cross-project-artifact-downloads-with-needs
[trigger]: https://docs.gitlab.com/ee/ci/yaml/README.html#trigger

## How to use it?

In order to use this yum repository via Ansible, add the following task during
system provisioning.

```yaml
yum_repository:
  name: cki
  description: "CKI - $basearch"
  baseurl: "https://gitlab.com/cki-project/yum-repo/-/jobs/artifacts/main/raw/repo/$basearch/?job=create-repo"
  enabled: true
  gpgcheck: false
```

To select only docker or gitlab-runner packages, use `create-repo-docker` or
`create-repo-gitlab-runner` instead of `create-repo` above.

> **NOTE:** The packages are built for **Fedora 33**.

## Packages

Right now the packages available at this yum repository are:

* **docker-ce - 20.10.2**
* **docker-ce-cli - 20.10.2**
* **docker-ce-rootless-extras - 20.10.2**
* **gitlab-runner - 13.9.0**

Those packages are mainly rebuilds of the upstream projects. The reasons to do
it are either Fedora 33 wasn't supported yet or some architecture was missing.

## Add more packages

To make easier to add new packages, there is a `gitlab-ci` template here called
`build-package.yml` that can be included at other projects.

The template has a few jobs for building the source and binary packages. It also
will trigger this pipeline, to update the yum repository.

The binary packages will be built for `amd64`, `arm64`, `ppc64le` and `s390x`
on native machines (Gitlab runners in [Beaker]).

The only requirements for the project are:

* The repo must be a package source (rpms) one and have a .spec file.
* The repo must be part of the [CKI Project] group.

> **NOTE:** Using artifacts between projects is a premium feature ([needs]).
> The [CKI Project] uses it because it is part of the [Gitlab Open Source program].

[Beaker]: https://beaker.engineering.redhat.com/
[CKI Project]: https://gitlab.com/cki-project
[Gitlab Open Source program]: https://about.gitlab.com/solutions/open-source/

### Build the packages

To build the packages you need to add a `.gitlab-ci.yml` file with:

```yaml
include:
  project: 'cki-project/yum-repo'
  file: 'build-packages.yml'
```

### Add the packages to the repo

To add the packages to this repo, you need to update the `.collect` templates
in `.gitlab-ci.yml`.

Example with a package from the project `cki-project/foo` built in the job
`build-rpm`:

```yaml
collect-foo:
  extends: .collect
  needs:
    - project: cki-project/foo
      job: build-rpm
      ref: main

# to create a specific repo
create-repo-foo:
  extends: .create-repo
  needs: [collect-foo]

# to include the packages in the global repo
create-repo:
  extends: .create-repo
  needs: [..., collect-foo]
```
